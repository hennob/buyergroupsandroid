package dankospark.koopgroepe;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Elzaan on 2017/05/30.
 */

public interface KoopGroepAPIEndpointIF {

        @Headers("Content-Type: application/json")
        @GET("Login")
        Call<UserProfile> login(@Query("username") String username, @Query("password") String password);

}
